Source: joypy
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Nilesh Patra <nilesh@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-setuptools,
               python3-all,
               python3-matplotlib,
               python3-numpy,
               python3-pandas (>= 0.20.0),
               python3-scipy,
               python3-pytest
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/joypy
Vcs-Git: https://salsa.debian.org/python-team/packages/joypy.git
Homepage: https://github.com/sbebo/joypy
Rules-Requires-Root: no

Package: python3-joypy
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-matplotlib,
         python3-numpy,
         python3-pandas (>= 0.20.0),
         python3-scipy
Suggests: r-cran-ggridges,
          r-cran-ggjoy
Description: ridgeline-/joyplots plotting routine
 JoyPy is a one-function Python package based on matplotlib + pandas
 with a single purpose: drawing joyplots (a.k.a. ridgeline plots).
 Joyplots are stacked, partially overlapping density plots.
 They are a nice way to plot data to visually compare distributions,
 especially those that change across one dimension (e.g., over time).
 Though hardly a new technique, they have become very popular lately
 thanks to the R packages ggridges and ggjoy.
